import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Text,
} from 'react-native';

import {MainView} from '../component/CustomView.js';

const {width, height} = Dimensions.get('window');
const Splash = ({navigation, route}) => {
  return (
    <MainView>
      {/* <StatusBarLight /> */}
      {/* <ImageBackground
        source={require('../images/splash-screen.png')}
        style={styles.container}></ImageBackground> */}
    </MainView>
  );
};

export default Splash;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainLogo: {
    height: 170,
    width: 210,
    resizeMode: 'contain',
    position: 'absolute',
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 'auto',
    marginBottom: 20,
  },
  text: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 1,
    color: '#FFFFFF',
  },
  bottomLogo: {
    height: 45,
    width: 100,
    resizeMode: 'contain',
    marginLeft: 5,
  },
});
