import React from 'react';
import {SafeAreaView} from 'react-native';
export const MainView = props => (
  <SafeAreaView style={{flex: 1, backgroundColor: 'white'}} {...props} />
);
